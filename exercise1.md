# Chrome DevTools

Chrome DevTools – это набор инструментов, встроенных в браузер Google Chrome, для создания и отладки сайтов. С их помощью можно просматривать исходный код сайта, отлаживать работу frontend: HTML, CSS и JavaScript. Также DevTools позволяет проверять сетевой трафик, быстродействие сайта и многое другое.

### Горячие клавиши, которыми можно открыть Chrome DevTools

1) Сочетанием горячих клавиш:
 
* Ctrl + Shift + I 

* Ctrl + Shift + C

* F12

2) В контекстном меню: на странице в любом месте кликнуть правой кнопкой мыши и выбрать «Просмотреть код». Главное – не путать с «Просмотр кода страницы», эта опция покажет исходный код вне DevTools.

3) Через меню браузера: в правом верхнем углу нажать на три точки, в меню выбрать раздел «Дополнительные инструменты» и далее «Инструменты разработчика».

### Вкладки Chrome DevTools

* **Elements.** Используется для выбора и редактирования любых HTML элементов на странице. Позволяет свободно манипулировать DOM и CSS.

Вкладка содержит две кнопки: Выбор элемента с помощью курсора и Переключение в режим выбора устройств, она пригодится при разработке адаптивных интерфейсов, мобильных версий сайтов или для тестирования страниц с разным разрешением монитора. При выборе любого DOM элемента на вкладке Styles будет отображаться все CSS правила применяемые к нему, в том числе и неактивные. Все правила разбиты на блоки и упорядочены по убыванию специфичности селектора. Можно на лету менять значения, деактивировать и дописывать новые правила и смотреть как это влияет на отображение. Также для выбранного элемента DOM доступно еще несколько вкладок: Event Listeners – содержит все события относящиеся к данному элементу, DOM Breakpoints – точки останова для элемента и Properties – список всех свойств для элемента. Также могут быть дополнительные вкладки добавляемые расширениями для Chrome.

*Ключевые возможности:*

1) Просмотр и редактирование в лайв режиме любого элемента DOM.

2) Просмотр и изменение CSS правил применяемых к любому выбранному элементу в панели Styles.

3) Просмотр всего списка событий и свойств для элемента на соответствующих вкладках.

* **Console.** Необходима для логирования диагностической информации в процессе разработки или взаимодействие с JavaScript на странице. Также все ошибки в JavaScript коде, будут выводится здесь с указанием файла и конкретного места в нем где произошла ошибка. Так же в консоль можно выводить XHR запросы. Есть возможность сохранять логи в отдельный файл.
Консоль содержит ряд инструментов и настроек для фильтрации выводимых сообщений, очистки консоли и запрета очистки логов при перезагрузке страницы – Preserve log.

Также консоль может отобразить/скрыть в виде отдельной вкладки находясь на любой другой вкладке и не покидая ее нажав клавишу Esc.

*Ключевые возможности:*

1) Использование консоли как отдельной панели или как окна рядом с любой другой панелью.

2) Возможность группировать большое колличество сообщения или выведите их на отдельных строках.

3) Очистка всех логов или сохранения их между перезагрузкой страниц, сохранение логов в отдельный файл.

4) Фильтрация по типу сообщения или по регулярному выражению.

5) Логирование XHR запросов.


* **Sources.** Инструмент Sources представляет собой своего рода IDE, где мы можем посмотреть все файлы подключенные на нашей странице. Мы можем посмотреть их содержимое, отредактировать код, скопировать его или сохранить измененный файл, как новый файл. Данную вкладку можно использовать и как полноценный редактор кода подключаясь к локальным файлам через Workspaces.

Также Sources используется для отладки JavaScript используя брейкпоинты. Для работы с брейкпоинтами предусмотрено большое количество специальных кнопок и доп. возможностей о которых больше можно узнать в официальной документации.

*Ключевые возможности:*

1) Отладка Вашего кода с помощью брейкпоинтов.

2) Использование браузера в качестве IDE с помощью Workspaces.

3) Запуск сниппетов с любой страницы.

* **Network.** Позволяет мониторить процесс загрузки страницы и всех файлов которые подгружаются при загрузке. Ее удобно использовать для оптимизация загрузки страниц и мониторинг запросов.
На панели отображается таблица всех запросов к данным и файлам, над ней располагаются кнопки для фильтрации нужных Вам запросов, очистки таблицы или включения/отключения записи запросов, кнопки управления отображением таблицы. Также есть дополнительные переключатели: Preserve log – не очищать таблицу при перезагрузке страницы, Disable cache – отключить кэш браузера (будет работать только при открытом Dev Tools), Offline – эмулирует отсутствие интернета, также соседний переключатель позволяющий эмулировать скорость скачивания/загрузки данных и ping для различных типов сетей.

*Ключевые возможности:*

1) Возможность отключить кэширование или установление ограничения пропускной способности.

2) Получение подробной таблицы с информацией о каждом запросе.

3) Фильтрация и поиск по всему списку запросов.

* **Perfomance.** Панель отображает таймлайн использования сети, выполнения JavaScript кода и загрузки памяти. После первоначального построения графиков таймлайн, будут доступны подробные данные о выполнение кода и всем жизненном цикле страницы. Будет возможно ознакомится с временем исполнения отдельных частей кода, появится возможность выбрать отдельный промежуток на временной шкале и ознакомится с тем какие процессы происходили в этот момент.

Инструмент применяется для улучшение производительности работы Вашей страницы в целом.

*Ключевые возможности:*

1) Возможность сделать запись чтобы проанализировать каждое событие, которое произошло после загрузки страницы или взаимодействия с пользователем.

2) Возможность просмотреть FPS, загрузку CPU и сетевые запросы в области Overview.

3) Щелкните по событию в диаграмме, чтобы посмотреть детали об этом.

4) Возможность изменить масштаб таймлайн, чтобы сделать анализ проще.

* **Memory.** Здесь расположено несколько инструментов, которые помогают отслеживать, какую нагрузку на систему оказывает выполнение кода:

Heap Snapshot. С помощью него можно посмотреть, как распределяется память между объектами JavaScript и связанными с ними элементами DOM.
Allocation instrumentation on timeline. Этот инструмент используется для устранения утечек памяти. Он показывает, как распределяется память между переменными в коде.
Allocation sampling. Профайлер записывает, как распределяется память на отдельные функции JavaScript.

*Ключевые возможности:*

1) Исправление проблем с памятью.

2) Профилирование CPU при работе с JavaScript.

* **Application.** Вкладка для инспектирования и очистки всех загруженных ресурсов, включая IndexedDB или Web SQL базы данных, local и session storage, куков, кэша приложения, изображений, шрифтов и таблиц стилей.

*Ключевые возможности:*

1) Быстрая очистка хранилищ и кэша.

2) Инспектирование и управление хранилищами, базами данных и кэшем.

3) Инспектирование и удаление файлов cookie.

* **Security.** На вкладке можно ознакомится с протоколом безопасности при его наличии и просмотреть данные о сертификате безопасности, если он есть.

Инструмент используется для отладки проблем смешанного контента, проблем сертификатов и прочее.

*Ключевые возможности:*

1) Окно Security Overview быстро подскажет безопасна ли текущая страница или нет.

2) Возможность просмотреть отдельные источники, чтобы просмотреть соединение и детали сертификата (для безопасных источников) или узнать, какие запросы не защищены (для небезопасных источников).

* **Lighthouse.** На этой вкладке можно проверить производительность сайта.

*Performance.* Позволяет узнать скорость загрузки сайта. Итоговый показатель зависит от времени загрузки интерактивных элементов, шрифтов и прочего контента, а также от времени блокировки и отрисовки стилей.

*Progressive Web App.* Позволяет проверить, регистрирует ли сайт Service Workers, возможна ли работа сайта офлайн, а также возвращает ошибку 200.

*Best Practices.* Помогает проверить безопасность сайта и узнать, применяются ли современные стандарты веб-разработки. На показатель влияет использование устаревших API, HTTPS, корректность кодировки и многое другое.

*Accessibility.* Позволяет узнать, насколько удобен сайт, как воспринимается контент и можно ли управлять интерфейсом и передвигаться по сайту без мыши.

*SEO.* Позволяет понять, насколько соблюдаются рекомендации Google по оптимизации сайта. На показатель влияют использование метатегов, наличие alt у изображений, адаптивная верстка и пр.

Каждый из показателей оценивается по шкале 100 баллов. Также для удобства оценка имеет цвет: зеленый – от 90 до 100 баллов, оранжевый – от 50 до 89 баллов, красный – ниже 49 баллов.

### Функциональные возможности:

* Кнопка выбора элемента на сайте (Ctrl + Shift + С).<br>
Чтобы выбрать HTML тег, который хотим изменить,нужно сделать выбор с помощью курсора мыши в появившемся окне, в закладке "Инспектор".В инспекторе нужно найти интересующий тег и кликнуть по нему, чтобы выделить. Но искать так долго, поэтому для упрощения поиска можно воспользоваться функцией выбора элемента прямо из дизайна сайта. Для этого найдите с левом верхнем углу иконку выбора элемента курсором или нажмите комбинацию клавиш Ctrl + Shift + C . Теперь можно выбрать курсором любой элемент на странице сайта и кликнуть на него.Тогда в окне инспектора будет автоматически найден этот элемент, а в окне "Инспектора" строчка с его HTML кодом будет подсвечена. Кликните на эту строчку правой кнопкой мыши, чтобы выбрать пункт "Править как HTML".В открывшемся текстовом поле вы сможете увидеть HTML код элемента и исправить его. Можно использовать HTML теги.
 
* Кнопка перехода к режиму эмуляции мобильных устройств (Ctrl + Shift + M).<br>
При создании адаптивных сайтов или веб-приложений полезно знать, как выглядит страница на планшете и мобильных устройствах. С помощью инструментов разработчика вы можете сделать это за несколько секунд. Для этого откройте Chrome Devtools, а затем кликните на иконку Toggle device toolbar в левом углу или нажмите комбинацию Ctrl+Shift+M.
Над страницей появится панель с режимами эмуляции из мобильных устройств и планшетов. По умолчанию панель инструментов открывается в режиме адаптивного окна просмотра. Чтобы изменить область до нужных размеров, задайте ширину или потяните за границы рабочей области. А если хотите увидеть, как страница отображается на конкретных устройствах, например, на iPhone 5, кликните на Responsive и выберите подходящий девайс.
На этой же панели есть еще одна полезная кнопка – DPR (Device Pixel Ratio). С её помощью проверяют, как выглядят изображения на ретина-дисплеях – экранах с повышенной плотностью. Чтобы посмотреть, как выглядит графика на разных устройствах, измените значение DPR и обновите страницу.

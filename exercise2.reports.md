# Ошибки во вкладке "Console" в DevTools на официальном сайте СберМегаМаркет <https://sbermegamarket.ru/>

## Баг №1. Запрос из постороннего источника заблокирован

__Серьезность:__ S5 Тривиальная (Trivial)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0.1

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на https://cms-res.online.sberbank.ru/sberid/BlackList/Button/No_Button.json. (Причина: не удалось выполнить запрос CORS). Код состояния: (null)

## Баг №2. Ошибка 400 Bad Request (Плохой запрос)

__Серьезность:__ S4 Незначительная (Minor)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - POST
https://sbermegamarket.ru/api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm=CFSEU+U/sVi8CpqccVLRGMBxAOEN9RBO3JTZTJoc6TAfgkRd7PVK6p+vWFXD4A4YEvbxOiEJ207Tm/KM17zJUG+F7PNefldpMdSkFqf55ltHRwRkseSgaUH1BrYk5LoNKLx4/Ffz6mCV/80oRJKfTmp9HCanIp/5X6OfuSk=
[HTTP/2 400 Bad Request 1208ms]


## Баг №3. Ошибка 502 Bad Gateway (Плохой шлюз)
__Серьезность:__ S4 Незначительная (Minor)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - GET https://sbermegamarketru.webim.ru/button.php  Состояние 502 Bad Gateway Версия HTTP/1.1 Передано2,12 кБ (размер 1,94 кБ) Referrer policystrict-origin-when-cross-origin Приоритет запроса Low

## Баг №4. Запрос из постороннего источника заблокирован

__Серьезность:__ S5 Тривиальная (Trivial)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0.1

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на https://cdn.retailrocket.ru/api/1.0/PushPartnerSettings/59f319fbc7d011dbaceaa05e?format=json. (Причина: отсутствует заголовок CORS «Access-Control-Allow-Origin»). Код состояния: 200.


## Баг №5. Заблокированный запрос
__Серьезность:__ S4 Незначительная (Minor)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0.1

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - Uncaught (in promise) Error: Blocked request browserHttpRequest https://extra-cdn.sbermegamarket.ru/static/dist/main.e7c8c8db.js:1

## Баг №6. Запрос из источника заблокирован
__Серьезность:__ S4 Незначительная (Minor)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0.1

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - Запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на https://mc.yandex.ru/watch/42694559?wmode=7&page-url=https%3A%2F%2Fsbermegamarket.ru%2F&charset=utf-8&uah=che%0A0&browser-info=pv%3A1%3Avf%3A7g4yzra6nxw2gnzhfy8utpb%3Afu%3A0%3Aen%3Autf-8%3Ala%3Aru-RU%3Av%3A1060%3Acn%3A1%3Adp%3A0%3Als%3A1366207200556%3Ahid%3A281520245%3Az%3A180%3Ai%3A20230621181922%3Aet%3A1687360762%3Ac%3A1%3Arn%3A516562957%3Arqn%3A78%3Au%3A1687186510652985657%3Aw%3A1583x747%3As%3A1600x900x24%3Ask%3A1%3Awv%3A2%3Ads%3A0%2C0%2C0%2C0%2C-475%2C0%2C%2C%2C%2C%2C%2C%2C%3Aco%3A0%3Acpf%3A1%3Ans%3A1687360761071%3Afip%3A95938a1acae647aba4ab907f9833eae3-0ed8ce9e1e39cec802dafc59181dfc61-a81f3b9bcdd80a361c14af38dc09b309-4bd84c89c35a312599d807af285e7b5f-8a4c66814aa955c22afa1996ca3000e0-00b2e6de4e7f2e69dd7de8ef95c7338a-61b9878bbce18de73aafc8582a198c0c-144c36ab74b4e0fd069733484ed37c20-a81f3b9bcdd80a361c14af38dc09b309-c6d7b47b2dcff33f80cab17f3a360d0b-861578da3666aba98730162cd5ac0199%3Arqnl%3A1%3Ast%3A1687360762%3At%3A%D0%9C%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D0%BF%D0%BB%D0%B5%D0%B9%D1%81%20%D0%A1%D0%B1%D0%B5%D1%80%D0%9C%D0%B5%D0%B3%D0%B0%D0%9C%D0%B0%D1%80%D0%BA%D0%B5%D1%82%20-%20%D0%BC%D0%B5%D1%81%D1%82%D0%BE%20%D0%B2%D1%8B%D0%B3%D0%BE%D0%B4%D0%BD%D1%8B%D1%85%20%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BE%D0%BA&t=gdpr(14)clc(0-0-0)rqnt(1)aw(1)ecs(0)fip(1)ti(2). (Причина: не удалось выполнить запрос CORS). Код состояния: (null).

## Баг №7. TypeError: NetworkError when attempting to fetch resource
__Серьезность:__ S4 Незначительная (Minor)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0.1

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - 
 fetch - https://extra-cdn.sbermegamarket.ru/static/dist/main.4782675b.js:1  
 InterpretGeneratorResume - self-hosted:1359
 AsyncFunctionThrow - self-hosted:760
 (Асинхронный: async)s - https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2
 
## Баг №8. Ошибка запроса/чтения последней версии приложения
__Серьезность:__ S4 Незначительная (Minor)

__Окружение:__  Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0.1

__Предшествующие условия:__
* открыта страница официального сайта СберМегаМаркета <https://sbermegamarket.ru/>

__Шаги воспроизведения:__
* на странице в любом месте кликнуть правой кнопкой мыши 
* в контекстном меню выбрать и нажать "Исследовать"
* откыть вкладку "Console" (Консоль) в DevTools

__Ожидаемый результат:__
* веб-приложение работает без ошибок

__Фактический результат:__

* в консоли зафиксирована следующая ошибка - 
page url: https://sbermegamarket.ru/; 
details: TypeError, NetworkError when attempting to fetch resource.
v https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2
error https://extra-cdn.sbermegamarket.ru/static/dist/main.4782675b.js:1
error https://extra-cdn.sbermegamarket.ru/static/dist/main.4782675b.js:1
errorFormatted https://extra-cdn.sbermegamarket.ru/static/dist/main.4782675b.js:1
handleError https://extra-cdn.sbermegamarket.ru/static/dist/main.4782675b.js:1
88522s https://extra-cdn.sbermegamarket.ru/static/dist/main.4782675b.js:1
https://extra-cdn.sbermegamarket.ru/static/dist/node_modules.89ca50aa.js:2

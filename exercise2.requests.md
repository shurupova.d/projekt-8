## С помощью вкладки "Network" (Сеть) в DevTools:

__Какой запрос(-ы) отправляется на сервер для получения списка типов товаров в шторке "Каталог"?__ 

* POST <https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/menu>

__Какой запрос(-ы) отправляется на сервер при использовании поиска товаров в каталоге?__ 

* POST <https://sbermegamarket.ru/api/mobile/v3/catalogService/catalog/searchSuggest>
* POST <https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/search>
* POST <https://sbermegamarket.ru/api/mobile/v1/catalogService/filters/search>
 
__Какой запрос(-ы) отправляется на сервер при поиске региона или города в модалке выбора вашего региона?__

* POST <https://sbermegamarket.ru/api/mobile/v1/addressSuggestService/address/suggest>
